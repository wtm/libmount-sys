use std::io::Write;
use bindgen;
use std::env;
use pkg_config;

fn main () {
  let cdir = std::env::current_dir().unwrap();
  let out_path = std::path::PathBuf::from(env::var("OUT_DIR").unwrap());

  let libmount = pkg_config::Config::new().probe("mount")
    .unwrap_or_else(|e| { eprintln!("pkg_config mount failed: {}", e); std::process::exit(1) });

  bindgen::builder()
    .clang_args(libmount.include_paths.iter()
      .map(|x| { let mut str = String::from("-I"); str.push_str(x.to_str().unwrap()); str }))
    .header_contents("wrapper.h", "#include <libmount.h>")
    .generate()
    .unwrap_or_else(|_| { eprintln!("bindgen libmount failed."); std::process::exit(1) })
    .write_to_file(out_path.join("bindings.rs")).unwrap();
}
