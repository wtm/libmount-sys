#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]

include!(concat!(env!("OUT_DIR"), "/bindings.rs"));

#[test]
fn test_mnt_new_context() {
	let ctx = unsafe { crate::mnt_new_context() };
	if ctx == 0 as *mut _ {
		panic!("mnt_new_context returned null.");
	}

	unsafe { crate::mnt_free_context(ctx) };
}
